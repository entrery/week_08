package ru.edu;

import ru.edu.db.CRUD;
import ru.edu.db.Record;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class IndexServlet extends HttpServlet {

    /**
     * Подключаем синглтон.
     */
    private CRUD crud = CRUD.getInstance();

    /**
     * Пример.
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws ServletException, IOException {

        List<Record> records = crud.getIndex();
        req.setAttribute("records", records);
        req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
    }

}
