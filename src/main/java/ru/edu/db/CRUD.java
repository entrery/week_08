package ru.edu.db;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CRUD {

    /**
     * select_all.
     */
    public static final String SELECT_ALL_SQL = "SELECT * FROM records";

    /**
     * by_id.
     */
    public static final String SELECT_BY_ID = "SELECT * FROM "
            + "records WHERE id=?";

    /**
     * insert.
     */
    public static final String INSERT_SQL = "INSERT INTO records (title, type, "
            + "text, price, publisher, email, phone, picture_url) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    /**
     * update.
     */
    public static final String UPDATE_SQL = "UPDATE records SET title=?, "
            + "type=?, text=?, "
            + "price=?, publisher=?, email=?, phone=?, "
            + "picture_url=? WHERE id=?";

    /**
     * count.
     */
    private static Long idCount = 0L;

    /**
     * crud.
     */
    private static CRUD instance;

    /**
     * data.
     */
    private DataSource dataSource;

    /**
     * construct.
     *
     * @param dataSource1
     */
    public CRUD(final DataSource dataSource1) {
        this.dataSource = dataSource1;
    }

    /**
     * set.
     * @param crud
     */
    public static void setInstance(final CRUD crud) {
        instance = crud;
    }


    /**
     * Синглтон.
     *
     * @return CRUD
     */
    public static CRUD getInstance() {
        synchronized (CRUD.class) {
            if (instance == null) {
                try {
                    Context ctx = new InitialContext();
                    Context env = (Context) ctx.lookup("java:/comp/env");
                    DataSource dataSource = (DataSource) env.lookup(
                            "jdbc/dbLink");
                    instance = new CRUD(dataSource);
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        }
        return instance;
    }

    /**
     * get index.
     *
     * @return query
     */
    public List<Record> getIndex() {
        return query(SELECT_ALL_SQL);
    }

    /**
     * query.
     *
     * @param sql
     * @param values
     * @return result
     */
    private List<Record> query(final String sql, final Object... values) {
        try (PreparedStatement statement =
                     getConnection().prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }
            ResultSet resultSet = statement.executeQuery();
            List<Record> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(map(resultSet));
            }
            return result;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    /**
     * map.
     *
     * @param resultSet
     * @return record
     */
    private Record map(final ResultSet resultSet) throws SQLException {
        Record record = new Record();
        record.setId(resultSet.getLong("id"));
        record.setTitle(resultSet.getString("title"));
        record.setType(resultSet.getString("type"));
        record.setPrice(new BigDecimal(resultSet.getString("price")));
        record.setText(resultSet.getString("text"));
        record.setPublisher(resultSet.getString("publisher"));
        record.setEmail(resultSet.getString("email"));
        record.setPhone(resultSet.getString("phone"));
        record.setPictureUrl(resultSet.getString("picture_url"));
        return record;

    }

    /**
     * get connect.
     *
     * @return data
     */
    private Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }

    }

    /**
     * get by id.
     *
     * @param id
     * @return records
     */
    public Record getById(final String id) {
        List<Record> records = query(SELECT_BY_ID, id);
        if (records.isEmpty()) {

            return null;
        }
        return records.get(0);
    }

    /**
     * execute.
     *
     * @param sql
     * @param values
     * @return statement
     */
    private Long execute(final String sql,
                         final Object... values) {

        try (PreparedStatement statement = getConnection(
        ).prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }

            Long id = null;
            statement.executeUpdate();

            ResultSet keys = statement.getGeneratedKeys();
            if (keys.next()) {
                id = keys.getLong(1);
            }
            return id;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);

        }
    }

    /**
     * save.
     *
     * @param record
     * @return record
     */
    public Long save(final Record record) {
        if (record.getId() == null) {
            Long id = idCount;
            idCount++;
            insert(record);
            return id;
        } else {
            update(record);
            return record.getId();
        }

    }

    /**
     * update.
     *
     * @param record
     * @return long
     */
    public Long update(final Record record) {

        String priceString = record.getPrice() == null
                ? new BigDecimal(0).toPlainString()
                : record.getPrice().toPlainString();

        return execute(UPDATE_SQL, record.getTitle(), record.getType(),
                record.getText(),
                priceString, record.getPublisher(),
                record.getEmail(), record.getPhone(),
                record.getPictureUrl(), record.getId());
    }


    /**
     * insert.
     *
     * @param record
     * @return insert record id
     */
    public Long insert(final Record record) {

        String priceString = record.getPrice() == null
                ? new BigDecimal(0).toPlainString()
                : record.getPrice().toPlainString();

        return execute(INSERT_SQL, record.getTitle(), record.getType(),
                record.getText(), priceString,
                record.getPublisher(), record.getEmail(),
                record.getPhone(), record.getPictureUrl());

    }

}
