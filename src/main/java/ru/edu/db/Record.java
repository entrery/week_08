package ru.edu.db;

import java.math.BigDecimal;


public class Record {

    /**
     * definition.
     */
    private Long id;

    /**
     * definition.
     */
    private String title;

    /**
     * definition.
     */
    private String type;

    /**
     * definition.
     */
    private BigDecimal price;

    /**
     * definition.
     */
    private String text;

    /**
     * definition.
     */
    private String publisher;

    /**
     * definition.
     */
    private String email;

    /**
     * definition.
     */
    private String phone;

    /**
     * definition.
     */
    private String pictureUrl;

    /**
     * get id.
     *
     * @return id
     */
    public Long getId() {

        return id;

    }

    /**
     * set id.
     *
     * @param id1
     */
    public void setId(final Long id1) {

        this.id = id1;

    }

    /**
     * get title.
     *
     * @return title
     */
    public String getTitle() {

        return title;

    }

    /**
     * set title.
     *
     * @param title1
     */
    public void setTitle(final String title1) {

        this.title = title1;

    }

    /**
     * get type.
     *
     * @return type
     */
    public String getType() {

        return type;

    }

    /**
     * set type.
     *
     * @param type1
     */
    public void setType(final String type1) {

        this.type = type1;

    }

    /**
     * get price.
     *
     * @return price
     */
    public BigDecimal getPrice() {

        return price;

    }

    /**
     * set price.
     *
     * @param price1
     */
    public void setPrice(final BigDecimal price1) {

        this.price = price1;

    }

    /**
     * get text.
     *
     * @return text
     */
    public String getText() {

        return text;

    }

    /**
     * set text.
     *
     * @param text1
     */
    public void setText(final String text1) {

        this.text = text1;

    }

    /**
     * get pub.
     *
     * @return pub
     */
    public String getPublisher() {

        return publisher;

    }

    /**
     * set pub.
     *
     * @param publisher1
     */
    public void setPublisher(final String publisher1) {

        this.publisher = publisher1;

    }

    /**
     * get email.
     *
     * @return email
     */
    public String getEmail() {

        return email;

    }

    /**
     * set email.
     *
     * @param email1
     */
    public void setEmail(final String email1) {

        this.email = email1;

    }

    /**
     * get phone.
     *
     * @return phone
     */
    public String getPhone() {

        return phone;

    }

    /**
     * set phone.
     *
     * @param phone1
     */
    public void setPhone(final String phone1) {

        this.phone = phone1;

    }

    /**
     * get pic.
     *
     * @return pic
     */
    public String getPictureUrl() {

        return pictureUrl;

    }

    /**
     * set pic.
     *
     * @param pictureUrl1
     */
    public void setPictureUrl(final String pictureUrl1) {

        this.pictureUrl = pictureUrl1;

    }

    /**
     * get short.
     *
     * @param length
     * @return textsub
     */
    public String getShort(final int length) {

        if (text.length() <= length) {

            return text;

        }

        return text.substring(0, length) + "...";

    }


}
