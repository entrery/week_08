<!DOCTYPE html>

<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ page import ="java.util.List" %>
<%@ page import ="ru.edu.db.Record" %>
<html>
<head>
    <jsp:include page="/WEB-INF/templates/header.jsp">
        <jsp:param name="title" value="Index"/>
    </jsp:include>
</head>

<body>

<div class="page">
    <div class="header">
        <h1>Доска объявлений</h1>
    </div>

    <div class="content">
        <%
        List<Record> records = (List<Record>) request.getAttribute("records");
        if(records.isEmpty()){ %>
          <h2>Записей нет</h2>
        <% }else{
          for(Record rec : records){
        %>
          <div class="record">
            <div class="img"><img src="<%=rec.getPictureUrl()%>?t=<%=java.time.Instant.now().getNano()+rec.getId()%>"/></div>
            <div class="title"><%=rec.getTitle()%></div>
            <div class="type"><%=rec.getType()%></div>
            <div class="short-description"><%=rec.getShort(300)%></div>
            <div class="price"><%=rec.getPrice().toPlainString()%></div>
            <div class="links">
              <a href="view?id=<%=rec.getId()%>">Просмотр</a> | <a href="edit?id=<%=rec.getId()%>">Редактировать</a>
            </div>

          </div>
        <% }
        } %>

    </div>
    <div class="button-container">
      <a href="edit">
        <span class="send-button">Разместить объявление</span>
      </a>
    </div>
    <%@ include file="/WEB-INF/templates/footer.jsp" %>

</div>
</body>
</html>
