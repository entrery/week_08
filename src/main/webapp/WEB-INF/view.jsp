<!DOCTYPE html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ page import ="ru.edu.db.Record" %>

<html>
<head>
  <!-- https://fonts.google.com/specimen/Roboto -->
  <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
  <link href="./style.css" rel="stylesheet" type="text/css"/>
</head>

<body>
  <div class="header">
    <h1>Доска объявлений</h1>
  </div>

  <%
 Record rec = (Record) request.getAttribute("record");
  %>

  <div class="content">
    <div class="advert">
      <div class="common-info">
        <img src="<%=rec.getPictureUrl()%>"/>
        <h2><%=rec.getTitle()%></h2>
        <h3><%=rec.getType()%></h3>
        <div class="price">
          <%=rec.getPrice().toPlainString()%> руб.
        </div>
      </div>


      <div class="adv-text">
        <h4>Текст объявления</h4>
        <p><%=rec.getText()%></p>
      </div>

      <div class="adv-contacts">
        <h4>Контактные данные</h4>
        <p>Автор: <span><%=rec.getPublisher()%></span></p>
        <p>Email: <span><%=rec.getEmail()%></span></p>
        <p>Телефон: <span><%=rec.getPhone()%></span></p>
      </div>

    </div>

    <div class="edit-btn">
      <a href="edit?id=<%=rec.getId()%>">Редактировать</a>
    </div>
  </div>

  <div class="footer">
    <a href="about">Об жизни</a>
  </div>

</body>

</html>
