<!DOCTYPE html>

<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ page import ="ru.edu.db.Record" %>
<%
  Record record = (Record) request.getAttribute("record");
  boolean isEdit = record != null;
 %>
<html>
<head>
    <jsp:include page="/WEB-INF/templates/header.jsp">
        <jsp:param name="title" value="Edit"/>
    </jsp:include>
</head>
<body>
<div class="page">

    <div class="header">
        <h1>Доска объявлений</h1>
    </div>

    <form method="post">
      <div class="content edit-form">
        <div class="form-item">
          <label for="title">Заголовок:</label>
          <input id="title" name="title" placeholder="Заголовок" value="<%=(isEdit ? record.getTitle(): "")%>"/>
        </div>

        <div class="form-item">
          <label for="type">Тип услуги:</label>
          <select id="type" name="type" placeholder="Тип" >
            <option "<%=isEdit && "Услуги".equals(record.getType()) ? "selected": ""%>">Услуги</option>
            <option "<%=isEdit && "Покупка".equals(record.getType()) ? "selected": ""%>">Покупка</option>
            <option "<%=isEdit && "Продажа".equals(record.getType()) ? "selected": ""%>">Продажа</option>
          </select>
        </div>

        <div class="form-item">
          <label for="text">Текст объявления:</label>
          <textarea id="text" name="text" placeholder="Текст объявления"><%=(isEdit ? record.getText(): "")%></textarea>
        </div>

        <div class="form-item">
          <label for="price">Цена:</label>
          <input id="price" name="price" placeholder="Цена" value="<%=(isEdit ? record.getPrice().toPlainString(): "")%>"/>
        </div>

        <div class="form-item">
          <label for="publisher">Автор:</label>
          <input id="publisher" name="publisher" placeholder="Автор" value="<%=(isEdit ? record.getPublisher(): "")%>"/>
        </div>

        <div class="form-item">
          <label for="email">Email:</label>
          <input id="email" name="email" placeholder="Email" value="<%=(isEdit ? record.getEmail(): "")%>"/>
        </div>

        <div class="form-item">
          <label for="phone">Телефон:</label>
          <input id="phone" name="phone" placeholder="Телефон" value="<%=(isEdit ? record.getPhone(): "")%>"/>
        </div>

        <div class="form-item">
          <label for="picture_url">Изображение:</label>
          <input id="picture_url" name="picture_url" placeholder="Изображение" value="<%=(isEdit ? record.getPictureUrl(): "")%>"/>
        </div>
      </div>

      <div class="button-container">
          <button class="send-button">Разместить объявление</button>
      </div>
    </form>

    <%@ include file="/WEB-INF/templates/footer.jsp" %>

</div>
</body>
</html>
