package ru.edu;


import org.junit.Test;

import ru.edu.EditServlet;
import ru.edu.db.CRUD;
import ru.edu.db.Record;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


public class EditServletTest {


    @Test
    public void doGet() throws ServletException, IOException {
        HttpServletRequest reqMock = mock(HttpServletRequest.class);
        HttpServletResponse respMock = mock(HttpServletResponse.class);
        CRUD crudMock = mock(CRUD.class);
        CRUD.setInstance(crudMock);
        when(reqMock.getParameter("id")).thenReturn("123");

        Record record = new Record();
        when(crudMock.getById("123")).thenReturn(record);

        EditServlet servlet = new EditServlet();
        RequestDispatcher requestDispatcherMock = mock(RequestDispatcher.class);
        when(reqMock.getRequestDispatcher("/WEB-INF/edit.jsp")).thenReturn(requestDispatcherMock);
        servlet.doGet(reqMock, respMock);
        verify(reqMock, times(1)).setAttribute("record", record);
        verify(requestDispatcherMock, times(1)).forward(reqMock, respMock);

    }



    @Test
    public void doPost() throws ServletException, IOException {

        HttpServletRequest reqMock = mock(HttpServletRequest.class);
        HttpServletResponse respMock = mock(HttpServletResponse.class);

        CRUD crudMock = mock(CRUD.class);
        CRUD.setInstance(crudMock);


        EditServlet servlet = new EditServlet();

        Map paramMap = spy(new HashMap());
        paramMap.put("id", param(String.valueOf(123L)));
        paramMap.put("title", param("title"));
        paramMap.put("type", param("type"));
        paramMap.put("text", param("text"));
        paramMap.put("price", param("1000"));
        paramMap.put("publisher", param("MockPublisher"));
        paramMap.put("email", param("email"));
        paramMap.put("phone", param("phone"));
        paramMap.put("picture_url", param("picture_url"));
        when(reqMock.getParameterMap()).thenReturn(paramMap);

        doAnswer((invocationOnMock)->{
            Record record = invocationOnMock.getArgument(0);

            assertEquals((Long)123L, record.getId());
            assertEquals("MockPublisher", record.getPublisher());

            return null;

        }).when(crudMock).save(any(Record.class));

        servlet.doPost(reqMock, respMock);
        verify(paramMap, times(1)).get("id");
        verify(crudMock, times(1)).save(any(Record.class));

    }


    @Test
    public void doPostRedirect() throws ServletException, IOException {
        HttpServletRequest reqMock = mock(HttpServletRequest.class);
        HttpServletResponse respMock = mock(HttpServletResponse.class);
        CRUD crudMock = mock(CRUD.class);
        CRUD.setInstance(crudMock);

        EditServlet servlet = new EditServlet();

        Map paramMap = spy(new HashMap());
        paramMap.put("id", param(String.valueOf(123L)));
        paramMap.put("title", param(null));
        paramMap.put("type", param("type"));
        paramMap.put("text", param("text"));
        paramMap.put("price", param("1000"));
        paramMap.put("publisher", param("MockPublisher"));
        paramMap.put("email", param("email"));
        paramMap.put("phone", param("phone"));
        paramMap.put("picture_url", param("picture_url"));

        when(reqMock.getParameterMap()).thenReturn(paramMap);
        doAnswer((invocationOnMock)->{
            Record record = invocationOnMock.getArgument(0);
            assertEquals((Long)123L, record.getId());
            assertEquals("MockPublisher", record.getPublisher());


            return null;
        }).when(crudMock).save(any(Record.class));
        servlet.doPost(reqMock, respMock);
        Record recordMock = spy(new Record());
        when(recordMock.getId()).thenReturn(123L);
        verify(respMock, times(1)).sendRedirect("view?id=" + recordMock.getId());

    }


    private String[] param(String param) {

        return new String[]{param};

    }

}