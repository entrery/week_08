package ru.edu;

import org.junit.Test;
import ru.edu.db.CRUD;
import ru.edu.db.Record;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;


public class IndexServletTest {

    @Test
    public void doGet() throws ServletException, IOException {

        CRUD crudMock = mock(CRUD.class);
        CRUD.setInstance(crudMock);

        List<Record> recordList = Collections.emptyList();
        when(crudMock.getIndex()).thenReturn(recordList);

        IndexServlet servlet = new IndexServlet();

        HttpServletRequest reqMock = mock(HttpServletRequest.class);
        HttpServletResponse respMock = mock(HttpServletResponse.class);

        RequestDispatcher requestDispatcherMock = mock(RequestDispatcher.class);
        when(reqMock.getRequestDispatcher("/WEB-INF/index.jsp")).thenReturn(requestDispatcherMock);

        servlet.doGet(reqMock, respMock);
        verify(reqMock, times(1)).setAttribute("records", recordList);
        verify(requestDispatcherMock, times(1)).forward(reqMock, respMock);

    }

}