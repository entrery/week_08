package ru.edu;

import org.junit.Test;
import ru.edu.db.CRUD;
import ru.edu.db.Record;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

public class ViewServletTest {

    @Test
    public void doGet() throws ServletException, IOException {
        HttpServletRequest reqMock = mock(HttpServletRequest.class);
        HttpServletResponse respMock = mock(HttpServletResponse.class);

        CRUD crudMock = mock(CRUD.class);
        CRUD.setInstance(crudMock);

        when(reqMock.getParameter("id")).thenReturn("123");

        Record record = new Record();
        when(crudMock.getById("123")).thenReturn(record);

        ViewServlet servlet = new ViewServlet();

        RequestDispatcher requestDispatcherMock = mock(RequestDispatcher.class);
        when(reqMock.getRequestDispatcher("/WEB-INF/view.jsp")).thenReturn(requestDispatcherMock);

        servlet.doGet(reqMock, respMock);
        verify(reqMock, times(1)).setAttribute("record", record);
        verify(requestDispatcherMock, times(1)).forward(reqMock, respMock);

    }

    @Test
    public void doGetRedirect() throws ServletException, IOException {

        HttpServletRequest reqMock = mock(HttpServletRequest.class);
        HttpServletResponse respMock = mock(HttpServletResponse.class);

        CRUD crudMock = mock(CRUD.class);
        CRUD.setInstance(crudMock);

        when(reqMock.getParameter("id")).thenReturn(null);

        Record record = new Record();
        when(crudMock.getById("123")).thenReturn(record);

        ViewServlet servlet = new ViewServlet();

        servlet.doGet(reqMock, respMock);
        verify(respMock, times(1)).sendRedirect("/index?error=id_not_found");

        when(reqMock.getParameter("id")).thenReturn("123");
        when(crudMock.getById("123")).thenReturn(null);

        servlet.doGet(reqMock, respMock);

        verify(respMock, times(1)).sendRedirect("/index?error=id_not_found");

    }

}