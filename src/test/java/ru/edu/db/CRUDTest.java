package ru.edu.db;

import org.junit.Before;
import org.junit.Test;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import java.math.BigDecimal;
import java.sql.*;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CRUDTest {

    private DataSource dataSourceMock = mock(DataSource.class);
    private Connection connectionMock = mock(Connection.class);
    private DataSource dataSource;
    private CRUD crud;
    private CRUD instance;

    private PreparedStatement selectByIdStatement = mock(PreparedStatement.class);
    private PreparedStatement insertStatement = mock(PreparedStatement.class);
    private PreparedStatement selectAllStatement = mock(PreparedStatement.class);
    private PreparedStatement updateStatement = mock(PreparedStatement.class);

    private ResultSet selectIdResultset = mock(ResultSet.class);
    private ResultSet insertResultset = mock(ResultSet.class);
    private ResultSet selectAllResultset = mock(ResultSet.class);
    private ResultSet updateResultset = mock(ResultSet.class);

    private Record create = mock(Record.class);
    private Record update = mock(Record.class);


    @Before
    public void setupJndi() throws NamingException, SQLException {

        System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.osjava.sj.SimpleContextFactory");

        System.setProperty("org.osjava.sj.jndi.shared", "true");

        Context ic = new InitialContext();

        ic.rebind("java:/comp/env", ic);
        ic.rebind("java:/comp/env/jdbc/dbLink", dataSourceMock);
        ic.rebind("jdbc/dbLink", dataSourceMock);

        Context ctx = new InitialContext();

        Context env = (Context) ctx.lookup("java:/comp/env");

        DataSource dataSource = (DataSource) env.lookup("jdbc/dbLink");

        assertEquals(dataSourceMock, dataSource);
        when(dataSource.getConnection()).thenReturn(connectionMock);

        when(connectionMock.prepareStatement(CRUD.SELECT_ALL_SQL)).thenReturn(selectAllStatement);
        when(connectionMock.prepareStatement(CRUD.SELECT_BY_ID)).thenReturn(selectByIdStatement);
        when(connectionMock.prepareStatement(CRUD.INSERT_SQL, Statement.RETURN_GENERATED_KEYS)).thenReturn(insertStatement);
        when(connectionMock.prepareStatement(CRUD.UPDATE_SQL, Statement.RETURN_GENERATED_KEYS)).thenReturn(updateStatement);


        when(selectAllStatement.executeQuery()).thenReturn(selectAllResultset);
        when(selectAllResultset.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(selectAllResultset.getString("id")).thenReturn("10").thenReturn("11");
        when(selectAllResultset.getString("title")).thenReturn("Test1").thenReturn("Test2");
        when(selectAllResultset.getString("type")).thenReturn("Test1").thenReturn("Test2");
        when(selectAllResultset.getString("price")).thenReturn("10000").thenReturn("20000");
        when(selectAllResultset.getString("text")).thenReturn("Test1").thenReturn("Test2");
        when(selectAllResultset.getString("publisher")).thenReturn("publisherTest1").thenReturn("publisherTest2");
        when(selectAllResultset.getString("email")).thenReturn("emailTest1").thenReturn("emailTest2");
        when(selectAllResultset.getString("phone")).thenReturn("phoneTest1").thenReturn("phoneTest2");
        when(selectAllResultset.getString("picture_url")).thenReturn("pictureTest1").thenReturn("pictureTest2");

        when(selectByIdStatement.executeQuery()).thenReturn(selectIdResultset);
        when(selectIdResultset.next()).thenReturn(true).thenReturn(false);
        when(selectIdResultset.getLong("id")).thenReturn(13L);
        when(selectIdResultset.getString("title")).thenReturn("Test3");
        when(selectIdResultset.getString("type")).thenReturn("Test3");
        when(selectIdResultset.getString("price")).thenReturn("30000");
        when(selectIdResultset.getString("text")).thenReturn("Test3");
        when(selectIdResultset.getString("publisher")).thenReturn("publisherTest3");
        when(selectIdResultset.getString("email")).thenReturn("emailTest3");
        when(selectIdResultset.getString("phone")).thenReturn("phoneTest3");
        when(selectIdResultset.getString("picture_url")).thenReturn("pictureTest3");

        when(insertStatement.executeQuery()).thenReturn(insertResultset);
        when(insertResultset.next()).thenReturn(true).thenReturn(false);
        when(insertStatement.getGeneratedKeys()).thenReturn(insertResultset);
        when(insertResultset.getLong(1)).thenReturn(10L);

        when(updateStatement.executeQuery()).thenReturn(updateResultset);
        when(updateResultset.next()).thenReturn(true).thenReturn(false);
        when(updateStatement.getGeneratedKeys()).thenReturn(updateResultset);
        when(updateResultset.getLong(1)).thenReturn(10L);
        crud = new CRUD(dataSource);

    }


    @Test
    public void test001_getInstance() {
        instance = crud.getInstance();
        assertNotNull(instance);
    }

    @Test
    public void test002_testGetById() {

        assertEquals("Test3", crud.getById("13").getTitle());

    }

    @Test
    public void test003_insert() {

        Long id = crud.insert(create);
        assertEquals(Long.valueOf(10L), id);

    }

    @Test
    public void test004_update() {
        long id = crud.update(update);
        assertEquals(10L, id);
    }

    @Test
    public void test005_selectAll() {
        List<Record> list = crud.getIndex();
        assertEquals(2, list.size());
        assertEquals(Long.valueOf(0), list.get(0).getId());
        assertEquals("Test1", list.get(0).getTitle());
        assertEquals("Test1", list.get(0).getType());
        assertEquals("Test1", list.get(0).getText());
        assertEquals("publisherTest1", list.get(0).getPublisher());
        assertEquals("emailTest1", list.get(0).getEmail());
        assertEquals("phoneTest1", list.get(0).getPhone());
        assertEquals("pictureTest1", list.get(0).getPictureUrl());
        assertEquals("publisherTest2", list.get(1).getPublisher());
        assertEquals(new BigDecimal(20000), list.get(1).getPrice());

    }


}