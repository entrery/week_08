package ru.edu.db;

import org.junit.Test;
import java.math.BigDecimal;
import static org.junit.Assert.*;

public class RecordTest {

    @Test
    public void advertTest() {

        Record record = new Record();

        record.setId(10L);
        record.setTitle("TitleTest");
        record.setType("TypeTest");
        record.setPrice(BigDecimal.valueOf(1012));
        record.setText("TextTest");
        record.setPublisher("PublisherTest");
        record.setEmail("EmailTest");
        record.setPhone("Phone");
        record.setPictureUrl("PictureURLTest");

        assertEquals(Long.valueOf(10L), record.getId());
        assertEquals("TitleTest", record.getTitle());
        assertEquals("TypeTest", record.getType());
        assertEquals(BigDecimal.valueOf(1012), record.getPrice());
        assertEquals("TextTest", record.getText());
        assertEquals("PublisherTest", record.getPublisher());
        assertEquals("EmailTest", record.getEmail());
        assertEquals("Phone", record.getPhone());
        assertEquals("PictureURLTest", record.getPictureUrl());

        assertEquals("Tex...", record.getShort(3));
        record.getShort(3);

        assertEquals("TextTest", record.getShort(50));

    }

}